function createWordle(json)
{
  var fill = d3.scale.category20();

  var dim1 = 1000;
  var dim2 = 800;

  var min_prob = d3.min(json, function(elem) { return elem.size; });
  var max_prob = d3.max(json, function(elem) { return elem.size; });
  console.log(min_prob);
  console.log(max_prob);

  var percent = function(num) { return (num - min_prob) / (max_prob - min_prob); }

  d3.layout.cloud().size([dim1, dim2])
      .words(json)
      .padding(3)
      .rotate(function() { return ~~(Math.random() * 2) * 90; })
      .font("Impact")
      .fontSize(function(d) { return (Math.log(percent(d.size)) + 10) * 10; })
      .on("end", draw)
      .start();

  function draw(words) {
    d3.select("body").append("svg")
        .attr("width", dim1)
        .attr("height", dim2)
      .append("g")
        .attr("transform", ["translate(", dim1/2, ",", dim2/2, ")"].join(""))
      .selectAll("text")
        .data(words)
      .enter().append("text")
        .style("font-size", function(d) { return d.size + "px"; })
        .style("font-family", "Impact")
        .style("fill", function(d, i) { return fill(i); })
        .attr("text-anchor", "middle")
        .attr("transform", function(d) {
          return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .text(function(d) { return d.text; });
  }
}
